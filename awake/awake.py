import os
import sys
import threading
import time
from datetime import datetime

import click
from pynput import keyboard, mouse

SECOND = 1
MINUTE = 60 * SECOND
HOUR = 60 * MINUTE


# ONLY READ/WRITE ONCE THE LOCK HAS BEEN AQUIRED
ts = int(time.time())
lock = threading.Lock()


def timestamp():
    now = datetime.now()
    return now.strftime("%H:%M:%S")


class KeepAwakeThread(threading.Thread):
    def __init__(self, interval, size, movement_duration):
        """A thread to keep the monitor awake

        Args:
            interval (float): time between awake checks.  The pointer is not
                moved if a keyboard or mouse event occurs more recently than
                `interval` seconds ago.
            size (int): Number of pixels to move during mouse movement.
            movement_duration (float): Time to execute the movement in seconds.
        """
        super().__init__()
        self.interval = interval
        self.size = size
        self.movement_duration = movement_duration
        self.mouse_controller = mouse.Controller()

    def run(self):
        while True:
            with lock:
                t = int(time.time()) - ts

            if t >= self.interval:
                self.move()
                time.sleep(self.interval)

            else:
                time.sleep(self.interval - t)

    def move(self):
        global ts
        dt = self.movement_duration / (4.0 * SECOND)
        print(f"    {timestamp()} Moving mouse ...")

        self.mouse_controller.move(self.size, 0)
        time.sleep(dt)
        self.mouse_controller.move(0, -self.size)
        time.sleep(dt)
        self.mouse_controller.move(-self.size, 0)
        time.sleep(dt)
        self.mouse_controller.move(0, self.size)
        time.sleep(dt)

        with lock:
            ts = int(time.time())


def on_keyboard_press(key):
    """Event handler for keyboard press events

    Args:
        key (Any): key pressed by the user
    """
    global ts

    with lock:
        ts = int(time.time())


def on_mouse_move(*args):
    """Event handler for mouse move events"""
    global ts

    with lock:
        ts = int(time.time())


def on_mouse_click(*args):
    """Event handler for mouse click events"""
    global ts

    with lock:
        ts = int(time.time())


def on_mouse_scroll(*args):
    """Event handler for mouse scroll events"""
    global ts

    with lock:
        ts = int(time.time())


@click.command()
@click.option(
    "--interval",
    default=1 * MINUTE,
    help="Time between mouse movements in seconds.",
)
@click.option("--size", default=100, help="Size of the movement in pixels.")
@click.option(
    "--movement-duration",
    default=1 * SECOND,
    help="Time to execute movement in seconds.",
    type=click.FLOAT,
)
def main(interval, size, movement_duration):
    """Keep your computer awake by periodically moving the mouse."""
    threads = [
        keyboard.Listener(on_press=on_keyboard_press),
        mouse.Listener(
            on_click=on_mouse_click,
            on_move=on_mouse_move,
            on_scroll=on_mouse_scroll,
        ),
        KeepAwakeThread(interval, size, movement_duration),
    ]

    for t in threads:
        t.daemon = True
        t.start()

    # Process for the main thread
    print("Keeping your computer awake.  Stop this proccess by ctrl+c.")
    while True:
        try:
            time.sleep(interval)
        except KeyboardInterrupt:
            print("\033[A")
            print("Closing program.")
            try:
                sys.exit(0)
            except SystemExit:
                os._exit(0)


if __name__ == "__main__":
    main()
