# Awake

A simple utility to keep your computer from locking by periodically moving your mouse.

```
Usage: awake [OPTIONS]

  Keep your computer awake by periodically moving the mouse.

Options:
  --interval INTEGER         Time between mouse movements in seconds.
  --size INTEGER             Size of the movement in pixels.
  --movement-duration FLOAT  Time to execute movement in seconds.
  --help                     Show this message and exit.
```

## Installation

1. Check out the git repository
2. Open a terminal in the repostory root directory
3. `pip install -e .`
